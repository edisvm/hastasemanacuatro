import {LitElement, html} from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class TarjetaDetalle extends LitElement {
    static get properties() {
        return {
            nomCliente:{type:String}, 
            nroTarjeta:{type:String}, 
            fechaCaduca:{type:String},
            categoria:{type:String},
            tipoTarjeta:{type:String}, 
            limiteTarjeta:{type:Number},
            saldoTarjeta:{type:Number},
            apagado:{type:Boolean} 
        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
    }

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
         <form class="row align-items-start">
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col col-md-6 col-sm-3 col-xs-12">
                <div class="d-flex justify-content-center">
                    <h3>Detalle de Tarjeta</h3>
                </div>
                <div class="form-group">
                    <label>Nombre del Cliente</label>
                    <span class="form-control">${this.nomCliente}</span> 
                </div>    
                <div class="form-group">                 
                    <label>Número de Tarjeta</label>
                    <span class="form-control">${this.nroTarjeta}</span> 
                </div> 
                <div class="form-group">
                    <label>Fecha de Caducidad</label>
                    <input type="date" name="fechaCaduca" value="${this.fechaCaduca}" 
                    @input="${this.updateFechaCaducidad}" class="form-control"></input>
                </div> 
                <div class="form-row">
                     <div class="form-group col-md-6">  
                         <label>Categoría</label>
                         <span class="form-control">${this.categoria}</span> 
                     </div>
                     <div class="form-group col-md-6">
                         <label>Tipo de Tarjeta</label>
                         <span class="form-control">${this.tipoTarjeta}</span> 
                     </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label>Limite S/. </label>
                        <span class="form-control">${this.limiteTarjeta}</span>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Saldo Actual S/. </label>
                        <span class="form-control">${this.saldoTarjeta}</span>
                    </div>
                </div>    
                <div class="form-group">
                    <label>Apagada </label>
                    <div class="custom-control custom-switch">
                       <input type="checkbox" class="custom-control-input" id="customSwitch1" checked>
                       <label class="custom-control-label" for="customSwitch1">SI</label>
                    </div>
                </div>
                <div class="text-center card-footer">
                   <button @click="${this.regresa}" class="btn btn-danger col-5"><strong>Volver</strong></button>
                </div>
            </div>
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
        </form>
        `;
    }

}

customElements.define('tarjeta-detalle', TarjetaDetalle);