import {LitElement,html,css} from 'lit-element';
import '../persona-main/persona-main.js';
/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class TarjetaPrincipal extends LitElement {
    static get properties() {
        return {

        }
    }

    static get styles() {
        return css`
              img{
                vertical-align: top;
              }

              /* header*/
              header{
                width: 100%;
                height: 600px;
                     background: linear-gradient(to bottom,
                  rgba(23, 46, 200, 0.9),
                  rgba(58, 115, 212, 0.55),
                  rgba(92, 168, 230, 0.48)
                  ), url(../img/pics/fondoedi.jpg);
                background-attachment: fixed;
                  background-position: center;
                  background-size: cover;
              }
              nav{
                width: 100%;
                position: fixed;
                box-shadow: 0 0 10px 0 rgba(0,0,0, .5)
              }

              .nav1{
                 background: transparent;
                 height: 80px;
                 color: #fff;
              }
              .nav2{
                 background: var(--fondo);
                 height: 100px;
                 color: #000;
              }
              .contenedor-nav{
                display: flex;
                margin: auto;
                width: 90%;
                justify-content: space-between;
                align-items: center;
                max-width: 1000px;
                height: inherit;
                overflow: hidden;
              }
              nav .enlaces a{
                display: inline-block;
                padding: 5px 0;
                margin-right: 17px;
                font-size: 17px;
                font-weight: 300;
                text-decoration: none;
                border-bottom: 3px solid transparent;
                color: inherit;
              }
              nav .enlaces a:hover{
                border-bottom: 3px solid #1498a4;
                transition: 0.6s;
                }
              
              .logo, .logo img{height: 80px;}
              
              .icono{
                display: none;
                font-size: 24px;
                padding: 23.5px 20px;
              }
              .textos{
                width: 100%;
                height: 100%;
                display: flex;
                justify-content: center;
                flex-direction: column;
                align-items: center;
                color: #fff;
                overflow: hidden;
                text-align: center;
              }

            
            `
    }
    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
         <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
         
         <header>
           <nav id="nav" class="nav1">
            <div class="contenedor-nav">
                <div class="logo">
                   <img vertical-align="top" height="80px" src="./img/logo1.jpg" alt="">
                </div>
                <div class="enlaces" id="enlaces">
                  <a href="#" id="enlace-inicio" class="btn-header">Inicio</a>
                  <a href="#" id="enlace-equipo" class="btn-header">Consulta</a>
                  <a href="#" id="enlace-servicio" class="btn-header">Alta</a>
                  <a href="#" id="enlace-trabajo" class="btn-header">recarga</a>
                  <a href="#" id="enlace-contacto" class="btn-header">Contacto</a>
                </div>
            </div>
        </nav>
    </header>
    <div>
        <persona-main></persona-main>
    </div>

        `;
    }

}

customElements.define('tarjeta-principal', TarjetaPrincipal);