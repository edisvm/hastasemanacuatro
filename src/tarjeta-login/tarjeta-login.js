import {LitElement, html} from 'lit-element';

/**
 * `LowerCaseDashedName` Description
 *
 * @customElement
 * @polymer
 * @demo
 * 
 */
class TarjetaLogin extends LitElement {
    static get properties() {
        return {
            email:{type:String}, 
            contraseña:{type:String}
        }
    }

    /**
     * Instance of the element is created/upgraded. Use: initializing state,
     * set up event listeners, create shadow dom.
     * @constructor
     */
    constructor() {
        super();
    }

    static get styles() {
        return 
            css`
            
            `
    }

    /**
     * Implement to describe the element's DOM using lit-html.
     * Use the element current props to return a lit-html template result
     * to render into the element.
     */
    render() {
        return html`
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
         <form class="row align-items-start">
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
            <div class="col col-md-6 col-sm-3 col-xs-12">
            <div class="d-flex justify-content-center">
                    <h3 class="card-title">Inicia Sesión</h3>
                </div>
               <div class="form-group">
                   <label for="exampleInputEmail1">Email del usuario</label>
                   <input type="email" class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Ej:Elena@nito.com">
                   <small id="emailHelp" class="form-text text-muted">Nunca compartiremos su correo electrónico con nadie más.</small>
                </div>
                <div class="form-group">
                   <label for="exampleInputPassword1">Contraseña</label>
                   <input type="password" class="form-control" id="InputPassword" >
                </div>
                <div class="form-check">
                   <input type="checkbox" class="form-check-input" id="exampleCheck1">
                   <label class="form-check-label" for="Check1">Recordar Datos</label>
                </div>
                <br/>
                <button type="submit" class="btn btn-primary">Ingreso</button>
            </div>
            <div class="col col-md-3 col-sm-3 col-xs-12"></div>
          </form>
        `;
    }

}

customElements.define('tarjeta-login', TarjetaLogin);